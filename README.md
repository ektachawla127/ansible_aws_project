# Ansible_AWS_Project

## Prerequisites:

- [X] **Python**
- [X] **boto3**
- [X] **Ansible**

## How to Run

**For Infra** 

Which Includes ec2_key , security group , 2 ec2 instances , elb.

```shell
ansible-playbook -i ec2.py infra.yaml --private-key <path_of_private-key>
```

**For Configuring Nginx**

Which includes copying index.j2 file to nginx root document of remote servers.

```shell
ansible-playbook -i ec2.py nginx.yaml --private-key <path_of_private-key>
```

NOTE: i) Private Ip is been shown on each webpages
ii) 5 minutes of pause is added in infra to make sure instances and intialized and ready with health checks done before configuring to elb.
iii) Make sure host_key_checking = False in ansible.cfg file.

## Screenshots:

- Instance 1 serving " Hello from EC2 Instance  server IP:<Private_IP>”

![enter image description here](images/1.png)




- Instance 2 serving " Hello from EC2 Instance  server IP:<Private_IP>”

![enter image description here](images/2.png)





- ELB serving instance 1 " Hello from EC2 Instance  server IP:<Private_IP>”

![enter image description here](images/3.png)




- ELB serving instance 2 " Hello from EC2 Instance  server IP:<Private_IP>”

![enter image description here](images/4.png)

